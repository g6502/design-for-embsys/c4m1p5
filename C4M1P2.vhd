library ieee;
use ieee.std_logic_1164.all;
--Binary To BCD
entity C4M1P2 is port
	(
		SW		: IN	STD_LOGIC_VECTOR (9 downto 0);
		HEX0 	: OUT STD_LOGIC_VECTOR (7 downto 0);
		HEX1 	: OUT STD_LOGIC_VECTOR (7 downto 0)
	);
end entity C4M1P2;

architecture C4M1P2_imp of C4M1P2 is 

signal V 	: std_logic_vector(4 downto 0) := SW(4 downto 0);
signal A 	: std_logic_vector(4 downto 0);	--
signal z 	: std_logic;	--
signal d0	: std_logic_vector(3 downto 0);	
signal d1	: std_logic_vector(3 downto 0);
	
begin

	z <= (V(3) and V(1)) or (V(3) and V(2));
--  A B C D E  --
--  4 3 2 1 0  --
-- d0 <= V when z = '0' else A;
--Val0   E
	d0(0) <= V(0);
--Val1  !A !B D + !B C D + !A B C !D + A !B !C !D + A B !C D
	d0(1) <= 
	(not V(4) and not V(3) and     V(1)) or 
	(not V(3) and     V(2) and     V(1)) or 
	(not V(4) and     V(3) and     V(2) and not V(1)) or 
	(    V(4) and not V(3) and not V(2) and not V(1)) or
	(    V(4) and     V(3) and not V(2) and     V(1));
--Val2  !A !B C + !A C D + A !C !D + A B !C
	d0(2) <= 
	(not V(4) and not V(3) and     V(2)) or 
	(not V(4) and     V(2) and     V(1)) or 
	(    V(4) and not V(2) and not V(1)) or 
	(    V(4) and     V(3) and not V(2));
--Val3  !A B !C !D + A !B !C D + A B C !D
	d0(3) <= 
	(not V(4) and     V(3) and not V(2) and not V(1)) or 
	(    V(4) and not V(3) and not V(2) and     V(1)) or 
	(    V(4) and     V(3) and     V(2) and not V(1));
--	d1 <= "0000" when z = '0' else "0001";
--Val0  !A B D + !A B C + B C D + A !B !C
	d1(0) <= 
	(not V(4) and     V(3) and     V(1)) or 
	(not V(4) and     V(3) and     V(2)) or 
	(    V(3) and     V(2) and     V(1)) or
	(    V(4) and not V(3) and not V(2));
--Val1   A C + A B
	d1(1) <= (V(4) and V(3)) or (V(4) and V(2));
	
	with d0 select
		HEX0 <=
	  --PGFEDCBA
		"11000000" when "0000",	--0
		"11111001" when "0001",	--1
		"10100100" when "0010",	--2
		"10110000" when "0011",	--3
		"10011001" when "0100",	--4
		"10010010" when "0101",	--5
		"10000010" when "0110",	--6
		"11111000" when "0111",	--7
		"10000000" when "1000",	--8
		"10011000" when "1001",	--9
		"11000000" when others;	--0
	with d1 select
		HEX1 <=
		"11000000" when "0000",	--0
		"11111001" when "0001",	--1
		"10100100" when "0010",	--2
		"10110000" when "0011",	--3
		"10011001" when "0100",	--4
		"10010010" when "0101",	--5
		"10000010" when "0110",	--6
		"11111000" when "0111",	--7
		"10000000" when "1000",	--8
		"10011000" when "1001",	--9
		"11000000" when others;	--0
--	HEX1 <= "11000000" when d1(0) = '0' else "11111001";
	
end architecture C4M1P2_imp;