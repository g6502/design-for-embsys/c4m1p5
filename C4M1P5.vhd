library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;

entity C4M1P5 is port
	(
		-- Input ports
		SW		: in  std_logic_vector(9 downto 0);
		-- Output ports
		HEX0	: out std_logic_vector(7 downto 0);
		HEX1	: out std_logic_vector(7 downto 0);
		HEX2	: out std_logic_vector(7 downto 0);
		HEX3	: out std_logic_vector(7 downto 0);
		HEX4	: out std_logic_vector(7 downto 0);
		HEX5	: out std_logic_vector(7 downto 0);
		LEDR	: out std_logic_vector(9 downto 0)
	);
end entity C4M1P5;

architecture C4M1P5_imp of C4M1P5 is 

	signal A :	std_logic_vector(4 downto 0) :=	"0" & SW(7 downto 4);
	signal B :	std_logic_vector(4 downto 0) :=	"0" & SW(3 downto 0);
	signal T0:	std_logic_vector(4 downto 0);
	signal Z0:	std_logic_vector(4 downto 0);
	signal C0:	std_logic :=	SW(8);
	signal C1:	std_logic;
	signal S0:	std_logic_vector(4 downto 0);
	signal S1:	std_logic;
	
-- Binary to BCD
	component C4M1P2 is port
		(
			SW		: IN	STD_LOGIC_VECTOR (9 downto 0);
			HEX0 	: OUT STD_LOGIC_VECTOR (7 downto 0);
			HEX1 	: OUT STD_LOGIC_VECTOR (7 downto 0)
		);
	end component C4M1P2;
	
begin 

	U1 : C4M1P2 port map (SW => ("00000" & A), HEX0 => HEX4, HEX1 => HEX5);
	U2 : C4M1P2 port map (SW => ("00000" & B), HEX0 => HEX2, HEX1 => HEX3);
	U3 : C4M1P2 port map (SW => ("000000000" & S1), HEX0 => HEX1);
	U4 : C4M1P2 port map (SW => ("00000" & S0), HEX0 => HEX0);
	
	process (SW) begin
		T0 <= A + B + C0;
		if T0 > 9 then 
			Z0 <= B"01010";
			C1 <= '1';
		else
			Z0 <= "00000";
			C1 <= '0';
		end if;
		S0 <= T0 - Z0;
		S1 <= C1;
	end process;
end architecture C4M1P5_imp;